package com.haba;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
/**Java Program to use Fibonacci numbers
 *
 *
 * @author Oleh Haba
 */
public class Application {
  /**Calculate Fibonacci numbers
   *
   * @param n count of Fibonacci numbers
   * @return Fibonacci numbers
   */
  private static int fib(final int n) {
    if (n <= 1) {
      return n;
    }
    return fib(n - 1) + fib(n - 2);
  }
  /** Main
   * \
   * @param args args
   */
  public static void main(String[] args) {
    Scanner inputNum = new Scanner(System.in,"UTF-8");
    System.out.println("Input interval:");
    int n = inputNum.nextInt(); /**@param n Enter the interval*/
    /** @param fibList ArrayList for save Fibonacci numbers*/
    ArrayList<Integer> fibList = new ArrayList<Integer>();
    /** @param oddList ArrayList for save Odd numbers*/
    ArrayList<Integer> oddList = new ArrayList<Integer>();
    /**@param evenList ArrayList for save Even numbers*/
    ArrayList<Integer> evenList = new ArrayList<Integer>();
    System.out.println("Fibonacci numbers:");
    for (int i = 0; i < n; i++) {
      fibList.add(fib(i));
      System.out.print(fibList.get(i) + "\t");
    }
    for (int i = 0; i < n; i++) {
      if (fibList.get(i) % 2 != 0) {
        oddList.add(fibList.get(i));
      } else if (fibList.get(i) % 2 == 0) {
        evenList.add(fibList.get(i));
      }
    }
    /**@param sumEven save sum of even numbers*/
    int sumEven = 0;
    /**@param sumOdd save sum of odd numbers*/
    int sumOdd = 0;
    System.out.println("\n" + "Even numbers from start:");
    for (int k : evenList) {
      sumEven += k;
      System.out.print(k + "\t");
    }
    System.out.println("\n" + "Odd numbers from end:");
    Collections.reverse(oddList);
    for (int k : oddList) {
      sumOdd += k;
      System.out.print(k + "\t");
    }
    System.out.println("\n" + "Sum of odd elements is: " + sumOdd);
    System.out.println("Sum of even elements is: " + sumEven);
    /**@param Collections.max(oddList) Max odd element*/
    System.out.println("Max odd element F1=" + Collections.max(oddList));
    /**@param Collections.max(evenList) Max even element*/
    System.out.println("Max even elements F2=" + Collections.max(evenList));
    /**@param oddPercent Percent of odd numbers*/
    double oddPercent = (double) oddList.size() / n * 100;
    /**@param evenPercent Percent of even numbers*/
    double evenPercent = (double) evenList.size() / n * 100;
    System.out.println("Odd percent is: " + Math.round(oddPercent) + "%");
    System.out.println("Even percent is: " + Math.round(evenPercent) + "%");

  }
}
